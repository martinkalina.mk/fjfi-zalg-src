program Strom;

type PrvekStromu = object
   data : integer;
   levy, pravy : ^PrvekStromu;
end

typedef UkPrvekStromu = ^PrvekStromu;

type Strom = object

   constructor vytvor();

   procedure vloz( data : integer );

   function najdi( data : integer ) : boolean;

   procedure odeber( data : integer );

   destructor smaz();

   function najdiPrvek( data : integer;
                        var prvek, predchudce : UkPrvekStromu ) : boolean;

   function najdiNejvetsiVLevemPodstromu( korenPodstromu : ^PrvekStromu ) : ^PrvekStromu;

   procedure smazPodstrom( podstrom : UkPrvekStromu );

   procedure vytvorPrvek( data : integer; var prvek : UkPrvekStromu );
   
   procedure napojPrvek( novyPrvek : UkPrvekStromu; var predchudce : UkPrvekStromu );

   koren : ^PrvekStromu;
end;

constructor Strom.vytvor()
begin
     koren := nill;
end

function Strom.najdiPrvek( data : integer;
                           var prvek, predchudce : UkPrvekStromu ) : boolean
begin
     najdiPrvek:=false;
     prvek:=koren;
     while prvek<>nil do
     begin
          if prvek^.data = data then
          begin
               najdiPrvek := true;
               break;
          end
          predchudce:=prvek;
          if prvek^.data> data then
             prvek:= prvek^.levy;
          else prvek:= prvek^.pravy;
     end;
end

procedure Strom.vytvorPrvek( data: integer; var prvek : UkPrvekStromu )
begin
     new( prvek );
     prvek^.data:=data;
     prvek^.levy:=nil;
     prvek^.pravy:=nil;
end

procedure Strom.napojPrvek( novyPrvek : UkPrvekStromu; var predchudce : UkPrvekStromu )
begin
   if predchudce = nill then
   begin
      koren := novyPrvek;
      exit;
   end
   if predchudce^.data < novyPrvek.data then
      { assert( predchudce^.pravy = nill) }
      predchudce^.pravy := novyPrvek      
   else
      { assert( predchudce^.levy = nill) }
      predchudce^.levy := novyPrvek;
end

procedure Strom.vloz( data : integer )
var prvek, predchudce: UkPrvekStromu
begin
  if koren = nil then
    begin
      new(koren);
      koren^.data:=data;
    end
  else
    begin
    if najdiPrvek( data, prvek, predchudce ) <> true then
    begin
         vytvorPrvek( data, prvek );
         {napojPrvek( prvek, predchudce );}
         if predchudce^.data> data then
         predchudce^.levy := prvek
         else predchudce^.pravy := prvek;
    end;
end;

function Strom.najdi( data : integer ) : boolean
var u1, u2 : UkPrvekStromu;
begin
   najdi := najdiPrvek( data, u1, u2 );
end;

destructor Strom.smaz()
begin
   smazpodstrom( koren );
end

procedure Strom.smazPodstrom( korenPodstromu : UkPrvekStromu )
begin
   if korenPodstromu <> nill then
   begin
      smazpodstrom( korenpodstromu^.levy );
      smazpodstrom( korenpodstromu^.pravy );
      dispose( korenpodstromu );
   end
end



