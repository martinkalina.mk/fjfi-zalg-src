program Strom;

type PrvekStromu = object
   data : integer;
   levy, pravy : ^PrvekStromu;
end

typedef UkPrvekStromu = ^PrvekStromu;

type Strom = object

   constructor vytvor();

   procedure vloz( data : integer );

   function najdi( data : integer ) : boolean;

   procedure odeber( data : integer );

   destructor smaz();

   function najdiPrvek( data : integer;
                        var prbvek, predchudce : UkPrvekStromu ) : boolean;

   function najdiNejvetsiVLevemPodstromu( korenPodstromu : ^PrvekStromu ) : ^PrvekStromu;

   procedure smazPodstrom( podstrom : UkPrvekStromu );

   procedure vytvorPrvek( data : integer; var prvek : UkPrvekStromu );

   koren : ^PrvekStromu;
end;

constructor Strom.vytvor()
begin
     koren := nill;
end

function Strom.najdiPrvek( data : integer;
                           var prvek, predchudce : UkPrvekStromu ) : boolean
begin
     najdiPrvek:=false;
     prvek:=koren;
     while prvek<>nil do
     begin
          if prvek^.data = data then
          begin
               najdiPrvek := true;
               break;
          end
          predchudce:=prvek;
          if prvek^.data> data then
             prvek:= prvek^.levy;
          else prvek:= prvek^.pravy;
     end;
end

procedure Strom.vytvorPrvek( data: integer; var prvek : UkPrvekStromu )
begin
     new( prvek );
     prvek^.data:=data;
     prvek^.levy:=nil;
     prvek^.pravy:=nil;
end

procedure Strom.napojPrvek( prvek : UkPrvekStromu; var predchudce : UkPrvekStromu )
begin
   if predchudce^.data> data then
      { assert( predchudce^.levy = nill ) }
      predchudce^.levy := prvek
   else
     { assert( predchudce^.pravy = nill ) }
     predchudce^.pravy := prvek;
end

procedure Strom.vloz( data : integer )
var prvek, predchudce: UkPrvekStromu
begin
  if koren = nil then
    begin
      new(koren);
      koren^.data:=data;
    end
  else
    begin
    if najdiPrvek( data, prvek, predchudce ) <> true then
    begin
         vytvorPrvek( data, prvek );
         napojPrvek( prvek, predchudce );
    end;
end;

function Strom.najdi( data : integer ) : boolean
var u1, u2 : UkPrvekStromu;
begin
   najdi := najdiPrvek( data, u1, u2 );
end;

destructor Strom.smaz()
begin
   smazpodstrom( koren );
end

procedure Strom.smazPodstrom( korenPodstromu : UkPrvekStromu )
begin
   if korenPodstromu <> nill then
   begin
      smazpodstrom( korenpodstromu^.levy );
      smazpodstrom( korenpodstromu^.pravy );
      dispose( korenpodstromu );
   end
end

procedure Strom.odeber( data : integer )
var prvek: UkPrvekStromu;
    predchudce: UkPrvekStromu;
begin
   if NajdiPrvek(data, prvek, predchudce) = true then
      smazPrvek( prvek, predchudce )
end;

procedure Strom.smazPrvek( prvek, predchudce: UkPrvekStromu )
begin
   if prvek^.levy=nil and prvek^.pravy=nil then
      SmazList(prvek, predchudce);
   else
      if prvek^.levy=nil or prvek^pravy=nil then
         SmazVeVetvi (prvek, predchudce);
      else SmazVeStromu (prvek, predchudce);
end

procedure Strom.SmazList (prvek, predchudce: UkPrvekStromu)
begin
  if prvek=koren then
    dispose(koren);
    koren:=nil;
  else
    if predchudce^.Levy=prvek then
       predchudce^.Levy:=nil;
    else
       { assert( predchudce^.pravy = prvek ) }
       predchudce^.Pravy:=nil;
    dispose(prvek);
end;

procedure Strom.SmazVeVetvi(prvek, predchudce: UkPrvekStromu)
var nasledovnik : UkPrvekStromu;
begin
   if prvek^.pravy = nil then
      nasledovnik:=prvek^.levy;
   else
      nasledovnik:=prvek^.pravy;

   if predchudce^.Levy=prvek then
       predchudce^.Levy:=nasledovnik;
   else
       { assert( predchudce^.pravy = prvek ) }
       predchudce^.Pravy:=nasledovnik;
    dispose(prvek);
end;

procedure Strom.SmazVeStromu( prvek: UkPrvekStromu )
var predchudce, nejvetsi: UkPrvekPodstromu
begin
   nejvetsi:=prvek^.levy;
   predchudce:prvek;
   while nejvetsi^.pravy <> nil do
   begin
      predchudce:=nejvetsi;
      nejvetsi:=nejvetsi^.pravy;
   end;
   prvek^.data:=nejvetsi^.data;
   smazPrvek( nejvetsi, predchudce );
end
