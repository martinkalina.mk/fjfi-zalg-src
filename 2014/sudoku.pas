
type Sudoku = object

   constructor vytvor;

   function zadejHodnotu( radek, sloupec, hodnota : integer ) : boolean;

   function vyres() : boolean;

   function res(radek, sloupec : integer): boolean;

   function najdiVRadku( radek, hodnota : integer ) : boolean;

   function najdiVeSloupci( sloupec, hodnota : integer ) : boolean;

   function najdiVeCtverci( radek, sloupec, hodnota : integer ) : boolean;

   function dalsiPolicko( var radek, sloupec : ineteger ) : boolean;

   var hodnoty : array[ 1..9 ][ 1..9 ] of integer;
       zadane  : array[ 1..9 ][ 1..9 ] of boolean;

end;

constructor Sudoku.vytvor
begin
   for radek = 1 to 9
     for sloupec = 1 to 9
     begin
        hodnoty[ radek ][ sloupec ] := 0;
        zadane[ radek ][ sloupec ] := false;
     end
end;

function Sudoku.zadejHodnotu( radek, sloupec, hodnota : integer ) : boolean
begin
  if najdiVRadku ( radek, hodnota ) = false and
     najdiVeSloupci ( sloupec, hodnota ) = false and
     najdiVeCtverci ( radek, sloupec, hodnota ) = false then
  begin
      hodnoty[ radek ][ sloupec ] := hodnota;
      zadane[ radek ][ sloupec ] := true;
      zadejHodnotu:= true;
  end
  else zadejHodnotu:= false;
  end;
end;

function Sudoku.najdiVRadku( radek, hodnota : integer ) : boolean
var sloupec:integer;
begin
   najdiVRadku:= false;
   for sloupec := 1 to 9 do
    begin
     if hodnoty [radek][sloupec] = hodnota then
      begin
      najdiVRadku := true;
      break;
      end;
    end;
end;

function Sudoku.najdiVeSloupci( sloupec, hodnota : integer ) : boolean
var radek:integer;
begin
   najdiVeSloupci:= false;
   for radek := 1 to 9 do
    begin
     if hodnoty [radek][sloupec] = hodnota then
      begin
      najdiVeSloupci := true;
      break;
      end;
    end;
end;

function Sudoku.najdiVeCtverci( radek, sloupec, hodnota : integer ) : boolean
var x:integer;
    y:integer;
    i:integer;
    j:integer;
begin
 najdiVeCtverci:= false;
 x:= ((radek-1) div 3);
 y:= ((sloupec-1) div 3);
 for i:=((3*x)+1) to ((3*x)+3) do
  for j:=((3*y)+1) to ((3*y)+3) do
  begin
    if hodnoty [i][j] = hodnota then
      begin
      najdiVeCtverci := true;
      exit;
      end;
  end;
end;


function Sudoku.vyres() : boolean
begin
   vyres := res( 1, 1 );
end;


function Sudoku.res(radek, sloupec : integer): boolean;
var hodnota : integer;
begin
  while zadane[radek][sloupec]=true do
  begin
     if dalsiPolicko( radek, sloupec ) = false then
     begin
        res := true;
        exit;
     end
  end

  for hodnota:=1 to 9 do
    begin
      if najdiVRadku( radek, hodnota)=false and
         najdiVeSloupci( sloupec, hodnota)=false and
         najdiVeCtverci( radek, sloupec, hodnota)=false then
      begin
        hodnoty[radek][sloupec]:=hodnota;
        if dalsiPolicko( radek, sloupec ) = false then
        begin
           res := true;
           exit;
        end
        res := res( radek, slupec );
      end;
    end;
    res := false;
end;

function Sudoku.dalsiPolicko( var radek, sloupec : ineteger ) : boolean
begin
   if radek=9 then
   begin
     if sloupec=9 then
     begin
       dalsiPolicko:=false;
       exit;
     end
     else
     begin
       sloupec:=sloupec+1;
       radek:=1;
     end;
   end
   else
   begin
     radek:=radek+1;
   end;
   dalsiPolicko := true;
end




