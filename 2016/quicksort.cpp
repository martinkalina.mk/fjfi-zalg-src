#include <cstdlib>

using namespace std;

void quicksort( int* pole, const int prvni, const int posledni )
{
    if (prvni >= posledni) return;
    int pivot = posledni;
    int leva = prvni;
    int prava = posledni - 1;
    do
    {
        while( pole[leva] <= pole[pivot] && leva < prava )
            leva ++;
        while( pole[prava] >= pole[pivot] && prava > leva )
            prava --;
        if( pole[prava] < pole[leva] ) prohod(&pole[leva], &pole[prava]);
        
    } while (prava > leva + 1);
    if( pole[prava] > pole[pivot] ) prohod(&pole[prava], &pole[pivot]);
    quicksort( pole, prvni, prava - 1 );
    quicksort( pole, prava + 1, posledni);
}


int main(int argc, char** argv) {

    return 0;
}

