#include <iostream>
using namespace std;
void vypis(int pole[], int pocet){
	cout << endl;
	for (int i = 0; i < pocet; i++){
		cout << pole[i] << "   ";
	}
	cout << endl;
}

void primy_vyber(int pole[], int pocet){
	int *nejmensi;
	int tmp;
	for (int i = 0; i < pocet - 1; i++){
		nejmensi = &(pole[i]);
		for (int j = i+1; j < pocet; j++)
			if (pole[j] < *nejmensi)
				nejmensi = &(pole[j]);
		tmp = pole[i];
		pole[i] = *nejmensi;
		*nejmensi = tmp;
	}
}

void prime_vkladani(int * pole, int pocet)
{
	int j = 1;
	int tmp;
	for (int i = 1; i < pocet; i++)
	{
		j = i;
		tmp = pole[j];
		while ( j > 0 && tmp < pole[j - 1])
		{
			pole[j] = pole[j - 1];
			j--;
		}
		pole[j] = tmp;
	}
}


void bublinkove(int *pole, int pocet)
{
	bool run = true;
	for (int i = 0; i < pocet - 1 && run; i++){
		run = false;
		for (int j = 0; j < pocet - i - 1; j++){
			if (pole[j]>pole[j + 1]){
				int tmp = pole[j];
				pole[j] = pole[j + 1];
				pole[j + 1] = tmp;
				run = true;
			}			
		}
	}
}

void treseni(int *pole, int pocet)
{
	int L = 0;
	int R = pocet-1;
	bool run = true;
	for (int i = 0; i < pocet - 1 && run; i++)
	{
		run = false;
		if (i %2==0)
		{
			//vpravo
			for (int j = L; j < R; j++)
			{
				if (pole[j]>pole[j + 1])
				{
					int tmp = pole[j];
					pole[j] = pole[j + 1];
					pole[j + 1] = tmp;
					run = true;
				}			
			}
			R--;

		}
		else 
		{
		//vlevo
			for (int j = R; j > L; j--)
			{
				if (pole[j - 1]>pole[j])
				{
					int tmp = pole[j - 1];
					pole[j - 1] = pole[j];
					pole[j] = tmp;
					run = true;
				}
			}
			L++;
		}


	}

	/*bool run = true;
	for (int i = 0; i < pocet - 1 && run; i++){
		run = false;
		for (int j = 0; j < pocet - i - 1; j++){
			if (pole[j]>pole[j + 1]){
				int tmp = pole[j];
				pole[j] = pole[j + 1];
				pole[j + 1] = tmp;
				run = true;
			}
		}
	}*/
}

void main(void){
	int pole[] = { 1, 5, 9, 15, 19, 6, 8, 3, 13, 17 };
	vypis(pole, 10);
	//primy_vyber(pole, 10);
	//prime_vkladani(pole, 10);
	//bublinkove(pole, 10);
	treseni(pole, 10);
	vypis(pole, 10);
	system("pause");
}
