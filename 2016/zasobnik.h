#ifndef MUJ_ZASOBNIK_H
#define MUJ_ZASOBNIK_H
struct prvek{
	int data;
	prvek *dalsi;
};

class zasobnik{
private: prvek *hlava;
public:	zasobnik();
		~zasobnik();
		void push(int data);
		int pull(void);
		void vypis(void);
		void vypis2(void);
};
#endif