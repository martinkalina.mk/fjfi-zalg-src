#include "Strom.h" 
#include <iostream>

using namespace std;

Strom::Strom()
{
	koren = nullptr;
}

bool Strom::Vloz(int D)
{ 
	Prvek *predchudce=nullptr;
	Prvek *hledany = nullptr;
	if (Najdi(D, koren, &predchudce, &hledany))
	{
		return false;
	}
	else
	{
		Prvek * tmp = new Prvek;
		tmp->Data = D;
		tmp->L = nullptr;
		tmp->P = nullptr;
		if (koren == nullptr) //DIVOKA PODMINKA
		{
			koren = tmp;
			cout << "Zapisuju koren " << tmp <<endl;
			return true;
		}
		else
		{
			if (predchudce->Data < D)
			{
				predchudce->P = tmp;
				cout << "zapisuju" << tmp << " do P " <<predchudce  << endl;

			}
			else
			{
				predchudce->L = tmp;
				cout << "zapisuju" << tmp << " do L " << predchudce << endl;

			}
			return true;
		}
	}

}

bool Strom::Smaz(int D)
{
	return true;
}

bool Strom::Najdi(int D)
{
	Prvek *predchudce=nullptr;
	Prvek *hledany = nullptr;
	return Najdi(D, koren, &predchudce, &hledany);
}

bool Strom::Najdi(int D, Prvek *podstrom, Prvek **predchudce, Prvek **hledany)
{
	if (podstrom != nullptr)
	{
		if (podstrom->Data == D)
		{
			(*hledany) = podstrom;
			return true;
		}
		(*predchudce) = podstrom;
		if (D < podstrom->Data)
			return Najdi(D, podstrom->L, predchudce, hledany);
		else
			return Najdi(D, podstrom->P, predchudce, hledany);

	}
	*hledany = nullptr;
	return false;

}


Strom::~Strom()
{

}