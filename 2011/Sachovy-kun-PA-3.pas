program sachovyKunCT;

type Sachovnice = object

     public

     {nuluje sachovnici}
     constructor vytvor;

     procedure zapisNaPolicko( i, j, hodnota : integer; );

     function nactiZPolicka( i, j: integer ) : integer;

     {Metoda vraci novy skok sachovym konem z policka o souradnicich i, j.
      Funkce osetruje skoky mimo sachovnici, ktere nevraci.
      Novy skok vraci v promenncyh i1, j1.
      V prvnim volani pozaduje smer = 0 a vraci vzdy skok dalsim moznym smerem.
      Pokud jiz neni kam skocit, vraci false, jinak true. }
     function skokKonem( i,j integer; var smer, i1, j1 : integer ) : boolean;


     private

     sachovnice = array[ 1..8, 1..8 ] of integer;
end;

function pole. skokKonem( i,j integer; var smer, i1, j1 : integer ) : boolean
begin
     while smer < 9 do
     begin
          smer := smer + 1;
          case smer of
          1 :  begin
              i1 := i + 1;
              j1 := j + 2;
          2 : begin
              i1 := i + 2;
              j1 := j + 1;

          if i1 >= 1 and i1 <= 8 and j1 >= 1 and j1 <= 8 then
          begin
               skokKonem := true;
               exit;
          end
     end
     skokKonem := false;
end


function prochazeniKonem( poziceI, poziceJ, krok : integer, var pole: Sachovnice ) : boolean
var pokus, novaPoziceI, novePoziceJ : boolean;
begin
  if krok = 65 then
     prochazeniKonem := true;
  else
  begin
     if( poziceI <= 8 and poziceI >= 1 and                 {zjistujeme, zda sem muzeme skocit}
         poziceJ <= 8 and poziceJ >= 1 and
         pole. nactizpolicka( poziceI, poziceJ ) = 0 ) then
     begin
          pole. zapisnapolicko( poziceI, poziceJ, krok );  {skaceme na policko}
          for smer=1 to 8 do                               {zkousime skoky vsemi smery}
          begin
               case smer of:
               1: begin
                  novaPoziceI:=poziceI-1
                  novaPoziceJ:=poziceJ+2
               2: begin
                  novaPoziceI:=poziceI+1
                  novaPoziceJ:=poziceJ+2

                  ....

               pokus := prochazeniKonem( novaPoziceI, novaPoziceJ, krok + 1, pole);
               if pokus = true then
               begin
                  prochazeniKonem:=true;
                  exit;
               end
          end;
          pole. zapisnapolicko(poziceI,poziceJ, 0);           {vracime se zpet}
     end;
  end
  prochazeniKonem:=false;
end.

function nejkratsiCestaVlna( startI, startJ, cilI, cilJ : integer,
                             var cestaI array [1..64] of integer,
                             var cestaJ array [1..64] of integer ) : integer
var indexvlny : integer;
    sach: sachovnice;
    smer, i1, j1, i, j, h;
    hledej : boolean;
begin
   indexvlny := 1;
   sach. vytvor;
   sach. zapisNaPolicko( startI, startJ, indexvlny ); {oznacime startovaci policko}
   repeat                                             {spustime vlnu}
      for i = 1 to 8 do
         for j = 1 to 8 do
         begin
            smer = 0;
            if nactiZPolicka( i, j ) = indexvlny then
               while skokKonem( i, j, smer, i1, j1 ) = true do
                  if sach. nactiZPolicka( i1, j1 ) = 0 then
                     sach. zapisNaPolicko( i1, j1, indexvlny + 1 );
         end;
      indexvlny = indexvlny + 1;
   until sach. nactiZPolicka( cilI, cilJ ) = 0;  {dokud vlna nedobehne na cilove policko}

   i := cilI;                                {extrahujeme nejkratsi cestu}
   j := cilJ;
   for h=1 to indexvlny do
   begin
      cestaI[ indexvlny - h + 1 ] := i;
      cestaJ[ indexvlny - h + 1 ] := j;
      smer = 0;
      hledej := true;
      while skokKonem(i,j,smer,i1,j1) = true and hledej = true do
         if sach. nactiZPolicka( i1, j1 ) = indexvlny - h then
         begin
              i := i1;
              j := j1;
              hledej := false;
         end
         {assert( sach. nactiZPolicka( i, j ) = indexvlny - h; }
   end
   cestaI[ 1 ] := i;
   cestaJ[ 1 ] := j;
end












