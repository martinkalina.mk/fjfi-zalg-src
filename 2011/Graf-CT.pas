program grafy;

type Graf = object
     public

     constructor vytvor( velikostGrafu : integer );

     function velikostGrafu() : integer;

     procedure vytvorHranu( u, v : integer, hodnota : real );

     function pocetSousedu( vrchol : integer ) :  integer;

     {vraci index n-teho souseda, kde n = 1 .. pocetSousedu}
     function vratSouseda( vrchol, n : integer ) : integer;

     function delkaHrany( u, v: integer ) : real;
end;

function vlnaVGrafu( cil : integer; var g : Graf, var delkyCest : array of real; var znacky : array of integer ) :  real/
var velikostG, i, j, pocetSousedu : integer
begin
     velikostG := g. velikostGrafu;

     while znacky[ cil ] <> 1 do
     begin
          for i := 1 to velikostG do
          if znacky[ i ] = 1 then
          begin
               pocetSousedu := g. pocetSousedu( i );
               for j := 1 to pocetSousedu do
               begin
                    Soused := vratSouseda( i, j );
                    if znacka[ Soused ] <> 1 then
                    begin
                       novaDelka = delkyCest[ i ] + delkaHrany( i, soused );
                       if delkyCest [ soused] > novadelka then delkyCest [soused] : = novadelka;
                       if znacka [ soused] = -1 then znacka [soused] : = 0;
                    end
               end
          end
     end
end

function nejkratsiCesta( start, cil : integer, var g : Graf, cesta : array of integer ) : real
var delkyCest : array of real;
    znacky : array of integer;
     {-1 = neznamy}
     { 0 = pokusny}
     { 1 = hotovy }
    velikostG : integer;
    i : integer;
begin
     velikostG := g. velikostGrafu();
     setSize( delkyCest, velikostG );
     setSize( znacky, velikostG );
     for i := 1 to velikostG do
     begin
          delkyCest[ i ] := realMax;
          znacky[ i ] := -1;
     end
     znacky[ start ] := 1;
     delkyCest[ start ] := 0;
     vlnaVGrafu( cil, g, delkyCest, znacky );
end

begin
end.

