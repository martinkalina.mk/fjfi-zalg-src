program GrafPA;

type Graf = object

     public

     constructor vytvor( velikostGrafu : integer );

     function velikostGrafu() : integer;

     procedure vytvorHranu( u, v : integer, hodnota : real );

     function pocetSousedu( vrchol : integer ) : integer;

     {vraci index n-teho souseda, kde n = 1 .. pocetSousedu}
     function vratSouseda( vrchol, n : integer ) : integer;

     function delkaHrany( u, v: integer ) : real;

     destructor smaz;
end;

function vlnaVGrafu( cil : integer, var g : Graf, delkyCest : array of real ) : real
         var i,j,soused,novadelka :integer;
             nejmensihodnota, posledniHotova :real
begin
   posledniHotova := 0.0;
   while delkyCest[cil]<> posledniHotova do
   begin
      nejmensihodnota := maxReal;
      for i :=1 to g. velikostGrafu() do
         if delkyCest[i] = posledniHotova then
            for j:=1 to g.pocetsousedu(i) do
            begin
               soused:= g.vratsouseda(i,j);
               if delkyCest[soused] > posledniHotova then
               begin
                  novadelka:= delkycest[i]+ g.delkahrany(soused,i);
                  if novadelka<delkycest[soused] then delkycest[soused]:= novadelka;
               end;
               if delkyCest[ soused ] < nejmensihodnota then
                  nejmensihodnota := delkyCest[ soused ];
            end;
      poslednoHotova := nejmensiHodnota;

     {for i :=1 to g. velikostGrafu() do
         if (znacky[i]=0) and (delkycest = nejmensihodnota) then
            znacky[i]:=1;}

   end;
end

function nejkratsiCesta( start, cil : integer, var g : Graf, cesta : array of integer ) : real
var delkyCest : array of real;
    znacky : array of integer;
     {-1 = neznamy}
     { 0 = pokusny}
     { 1 = hotovy }
    velikostG : integer;
    i : integer;
begin
     velikostG := g. velikostGrafu();
     setSize( delkyCest, velikostG );
     setSize( znacky, velikostG );
     for i := 1 to velikostG do
     begin
          delkyCest[ i ] := realMax;
          znacky[ i ] := -1;
     end
     znacky[ start ] := 1;
     delkyCest[ start ] := 0;
     vlnaVGrafu( cil, g, delkyCest, znacky );
end

begin
end.

