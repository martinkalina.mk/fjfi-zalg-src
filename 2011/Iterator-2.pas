type
 UkPrvekSeznamu = ^PrvekSeznamu;
 PrvekSeznamu = record
    data : integer
    nasledujici : ukPrvekSeznamu
 end;


type Seznam = object
begin

     public

     function prvniPrvek : IteratorSeznamu;

     procedure vloz( data: integer, pozice : IteratorSeznamu );

     procedure smaz( pozice : IteratorSeznamu );

     private

     hlavicka : ukPrvekSeznamu;

     posledni : ukPrvekSeznamu;

     pocetPrvku : integer;
end;


type Iterator = object

     function dalsi : boolean; virtual;

     function hodnota : integer; virtual;

     procedure smaz; virtual;
end

type IteratorSeznamu = object (Iterator)
     function dalsi : boolean;

     function hodnota : integer;

     procedure nastav( data : integer );

     private
     prvek : ukPrvekSeznamu;
end

function IteratorSeznamu. dalsi : boolean
begin
     if prvek <> nil then
        prvek := prvek^. nasledujici;
     if prvek <> nil then
        dalsi := true;
     else
        dalsi := false;
end

function IteratorSeznam. hodnota : integer
begin
     hodnota := prvek^. data;
end

type IteratorStromu = object (Iterator)
     function dalsi : boolean;

     function hodnota : integer;

     procedure nastav( data : integer );

     private
     vrchol : ukVrcholStromu;
end

function IteratorStromu. dalsi() : boolean
begin
     if vrchol <> nil then
     begin
          if vrchol^. levy <> nil then
             vrchol := vrchol^. levy;
          else
              if vrchol^. pravy <> nil then

          begin

          end
     end
end


procedure filtruj( i : Iterator )
begin
     repeat
           if i. hodnota > 10 then
              i. smaz;
     until i. dalsi = false;
end

program Iterator2;
var seznam, seznam2, seznam3 : Seznam;
    strom, strom1 : Strom;
    i : Iterator;
    i1 : IteratorSeznamu;
    i2 : IteratorStromu;
begin
     i = seznam. prvniPrvek;
     repeat
           writeln( " hodnota je ", i. hodnota );
           if i. hodnota < 10 then
              seznam. smaz( i );
     until i. dalsi = false;

    ....

    i1 = seznam. prvni;
    filtruj( i1 );
    i2 = strom. prvni;
    filtruj( i2 );

end.


