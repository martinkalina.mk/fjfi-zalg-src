
struct Sudoku
{
    Sudoku();
    
    bool nastavPrvek( int radek, int sloupec, int hodnota );
    
    bool najdiReseni();
    
    protected:
        
    bool krokZpet(int *radek, int *sloupec);
    
    bool pripustnaHodnota(int radek,int sloupec, int hodnota);
    
    int pole[ 9 ][ 9 ];
    
    bool zadane[ 9 ][ 9 ];
};

Sudoku::Sudoku()
{
    for( int i = 0; i < 9; i++ )
        for( int j = 0; j < 9; j++ )
        {
            pole[ i ][ j ] = 0;
            zadane[ i ][ j ] = false;
        }
}

bool Sudoku::nastavPrvek(int radek, int sloupec, int hodnota)
{
    if( ! pripustnaHodnota( radek, sloupec, hodnota ))
        return false;
    pole[ radek ][ sloupec ] = hodnota;
    zadane[ radek ][ sloupec ] = true;
}

bool Sudoku::najdiReseni()
{
    for (int i = 0; i < 9; i++)
        for (int j = 0; j < 9; j++)
        {
            if (zadane[i][j])
                continue;
            int hodnota = pole[i][j] + 1;
            
            while (hodnota <= 9 && !pripustnaHodnota(i, j, hodnota))
                hodnota = hodnota + 1;
            
            if (hodnota == 10)
            {
                if (!krokZpet(&i, &j))
                    return false;
            }
            else
                pole[i][j] = hodnota;
        }
    return true;
}

bool Sudoku::krokZpet(int *radek, int *sloupec)
{
    while (pole[*radek][*sloupec] == 9 || zadane[*radek][*sloupec])
    {
        if (*sloupec > 0)
            *sloupec = *sloupec - 1;
        else
        {
            if (*radek == 0)
                return false;
            *sloupec = 8;
            *radek = *radek - 1;
        }
    }
    if (*sloupec > 0)
        *sloupec = *sloupec - 1;
    else
    {
        *sloupec = 8;
        *radek = *radek - 1;
    }
    return true;
}

bool Sudoku::pripustnaHodnota(int radek,int sloupec, int hodnota)
{
    for (int i=0; i<9; i++)
        if ((pole[i][sloupec] == hodnota || pole [radek][i] == hodnota))
            return false;
    int rctverce = (radek / 3)*3;
    int sctverce = (sloupec / 3)*3;
    for (int i = rctverce; i < rctverce+3; i++)
        for (int j = sctverce; j < sctverce+3; j++)
            if (pole[i][j] == hodnota)
                return false;
}
