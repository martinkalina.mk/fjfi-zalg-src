const int rozmery;

struct Sachovnice
{
    
    Sachovnice()
    {
        for( int i = 0; i < rozmery; i++ )
           for( int j = 0; j < rozmery; j++ )
               sachovnice[ i ][ j ] = 0;
    }
    
    void oznacPole( int radek, int sloupec, int hodnota )
    {
        sachovnice[ radek ][ sloupec ] = hodnota;
    }
    
    void smazPole( int radek, int sloupec )
    {
        sachovnice[ radek ][ sloupec ] = 0;
    }
    
    int nactiPole( int radek, int sloupec )
    {
        return sachovnice[ radek ][ sloupec ];
            
    }
    int sachovnice[ rozmery ][ rozmery ];
};


struct SachovyKun
{
    
    bool resUlohu( int radek, int sloupec )
    {
        sachovnice.oznacPole(radek, sloupec, 1);
        if( ! skokKonem( radek, sloupec ) )
            return false;
        
        //TODO: Rekostrukce cesty
    }
    
    bool skokKonem (int radek, int sloupec)
    {
        if (sachovnice.nactiPole(radek, sloupec) == rozmery * rozmery)
        {
            return true;
        }
        for (int skok = 0; skok<8; skok++)
        {
            int cilovyradek;
            int cilovysloupec;
            vypoctiSkok(radek, sloupec, skok, &cilovyradek, &cilovysloupec);
            if (zkusSkok(cilovyradek, cilovysloupec))
            {
                sachovnice.oznacPole(cilovyradek, cilovysloupec, 
                                     sachovnice.nactiPole(radek, sloupec) + 1);
                if (skokKonem(cilovyradek, cilovysloupec) == true)
                {
                    return true;
                }
                else
                {
                    sachovnice.smazPole(cilovyradek, cilovysloupec);
                }
            }
        }
        return false;
    }
    void vypoctiSkok(int radek, int sloupec, int skok, int *cilovyradek, int *cilovysloupec)
    {
        int zmenaRadku[ 8 ] = { -2, -1, 1, 2, 2, 1, -1, -2};
        int zmenaSloupce [8] = {1, 2, 2, 1, -1, -2, -2, -1};
        *cilovyradek = radek + zmenaRadku[skok];
        *cilovysloupec = sloupec + zmenaSloupce[skok];
    }
    bool zkusSkok (int radek, int sloupec)
    {
        if ((radek < rozmery) && (radek >= 0) && (sloupec < rozmery) && (sloupec >= 0)
                && (sachovnice.nactiPole(radek, sloupec) == 0))
        {
            return true;
        }
        return false;
    }
    Sachovnice sachovnice;   
};


