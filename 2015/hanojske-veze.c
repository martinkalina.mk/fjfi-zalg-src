#include <assert.h>

const int maximalniPocetDisku = 1024;

struct Jehla
{
   int disky[ maximalniPocetDisku ];

   int pocetDisku;

   Jehla()
   {
      pocetDisku = 0;
   };

   void pridaniDisku( const int velikostDisku )
   {
      assert( pocetDisku == 0 || velikostDisku < disky[ pocetDisku - 1 ] );
      disky[ pocetDisku ] = velikostDisku;
      pocetDisku++;   
   };

   int odebraniDisku()
   {
      assert( pocetDisku > 0 );
      pocetDisku--;
      return disky[ pocetDisku ];
   }

   void vypis()
   {
       // TODO:
   }   

};

Jehla j1, j2, j3;

void vypisJehly()
{
   j1.vypis();
   j2.vypis();
   j3.vypis();
}

void resHanojskeVeze( const int pocetDisku, 
                      Jehla* zdrojova,
                      Jehla* cilova,
                      Jehla* odkladaci )
{
   if( pocetDisku == 0 )
      return;
   resHanojskeVeze( pocetDisku - 1, zdrojova, odkladaci, cilova );
   vypisJehly();
   cilova->pridaniDisku( zdrojova->odebraniDisku() );
   vypisJehly();
   resHanojskeVeze( pocetDisku - 1, odkladaci, cilova, zdrojova );
   vypisJehly();
}

int main( int argc, char* argv[] )
{
   const int pocetDisku = 5;
   for( int i = pocetDisku; i >= 1; i-- )
      j1.pridaniDisku( i );
   
   resHanojskeVeze( j1, j2, j3 );   
}