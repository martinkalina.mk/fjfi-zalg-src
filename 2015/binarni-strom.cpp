#include <assert.h>

struct UzelStromu
{
   int data;

   UzelStromu *levy, *pravy;

   UzelStromu( const int hodnota )
   {
      data = hodnota;
      levy = NULL;
      pravy = NULL;
   }
};

struct BinarniStrom
{
   UzelStromu *koren;

   BinarniStrom()
   {
      koren = NULL;
   }

   bool pridejPrvek( const int hodnota );

   bool najdiPrvek( const int hodnota );

   void smazPrvek( const int hodnota );

   ~BinarniStrom();

   
   /****
    * Pomocne metody
    */
   protected:
 
   
     
};