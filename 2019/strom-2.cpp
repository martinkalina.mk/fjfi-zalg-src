#include "pch.h"
#include <iostream>
//#define NDEBUG // vypne asserce
#include <assert.h>
struct Uzel
{
	Uzel(double d);

	double data;
	Uzel *levy, *pravy;
};

Uzel::Uzel(double d)
{
	data = d;
	levy = nullptr;
	pravy = nullptr;
}

struct BinarniStrom
{
	BinarniStrom();

	void vloz(double data);

	bool najdi(double data);

	void smaz(double data);

	~BinarniStrom();

	void vlozDoPodstromu(Uzel* podstrom, double data);

	void smazPodstrom(Uzel* podstrom);

	Uzel* najdiVPodstromu(Uzel* podstrom, double data, Uzel** predchudce);

	void smazList(Uzel* uzel, Uzel* predchudce );
	
	void smazVeVetvi(Uzel* uzel, Uzel* predchudce);

	void smazVeStromu(Uzel* uzel);

	Uzel* koren;
};

BinarniStrom::BinarniStrom()
{
	koren = nullptr;
}

void BinarniStrom::vloz(double data)
{
	if (koren == nullptr)
		koren = new Uzel( data );
	else
		vlozDoPodstromu(koren, data);
}

bool BinarniStrom::najdi(double data)
{
	Uzel* predchudce = nullptr;
	if(najdiVPodstromu(koren, data, &predchudce) != nullptr )
		return true;
	return false;
}

void BinarniStrom::smaz(double data)
{
	Uzel* predchudce=nullptr;
	Uzel* uzel = najdiVPodstromu(koren, data, &predchudce);
	if (predchudce == nullptr)
	{
		assert(uzel == koren);
		if (koren->levy == nullptr && koren->pravy == nullptr)
		{
			delete koren;
			koren = nullptr;
		}
		else
			if (koren->levy != nullptr && koren->pravy != nullptr)
				smazVeStromu(koren);
			else
			{
				if (koren->levy != nullptr)
					koren = koren->levy;
				else koren = koren->pravy;
				delete uzel;
			}
	}
	else
	{
		if (uzel->levy == nullptr && uzel->pravy == nullptr)
			smazList(uzel, predchudce);
		else if (uzel->pravy != nullptr && uzel->levy != nullptr)
			smazVeStromu(uzel);
		else
			smazVeVetvi(uzel, predchudce);
	}
}
void BinarniStrom::smazList(Uzel* uzel, Uzel* predchudce)
{
	assert(uzel->pravy == nullptr || uzel->levy == nullptr );
	assert(predchudce->pravy == uzel || predchudce->levy == uzel);
	if (predchudce->pravy == uzel)
		predchudce->pravy = nullptr;
	else
		predchudce->levy = nullptr;
	delete uzel;
}

void BinarniStrom::smazVeVetvi(Uzel* uzel, Uzel* predchudce)
{
	assert( ( uzel->pravy == nullptr && uzel->levy != nullptr ) || 
		    ( uzel->pravy != nullptr && uzel->levy == nullptr) );
	assert( predchudce->pravy == uzel || predchudce->levy == uzel );
	Uzel* nasledovnik;
	if (uzel->levy != nullptr)
		nasledovnik = uzel->levy;
	else
		nasledovnik = uzel->pravy;
	if (predchudce->levy == uzel)
		predchudce->levy = nasledovnik;
	else
		predchudce->pravy = nasledovnik;
	delete uzel;
}

void BinarniStrom::smazVeStromu(Uzel* uzel)
{
	assert(uzel->pravy != nullptr && uzel->levy != nullptr);
	Uzel* pomocny = uzel->levy;
	Uzel* predchudce = uzel;
	while (pomocny->pravy != nullptr)
	{
		predchudce = pomocny;
		pomocny = pomocny->pravy;
	}
	uzel->data = pomocny->data;
	if (pomocny->levy == nullptr)
		smazList(pomocny, predchudce);
	else
		smazVeVetvi(pomocny, predchudce);
}

void BinarniStrom::vlozDoPodstromu(Uzel* koren, double data)
{
	if (data < koren->data)
	{
		if (koren->levy != nullptr)
			vlozDoPodstromu(koren->levy,data);
		else
			koren->levy = new Uzel( data );
	}
	if (data > koren->data)
	{
		if (koren->pravy != nullptr)
			vlozDoPodstromu(koren->pravy, data);
		else
			koren->pravy = new Uzel(data);
	}
}

Uzel* BinarniStrom::najdiVPodstromu(Uzel* podstrom, double data, Uzel** predchudce)
{
	if (podstrom == nullptr)
		return nullptr;
	if (data == podstrom->data)
		return podstrom;
	*predchudce = podstrom;
	if (data < podstrom->data)
		return najdiVPodstromu(podstrom->levy, data, predchudce);
	else
		return najdiVPodstromu(podstrom->pravy, data, predchudce);
}


BinarniStrom::~BinarniStrom()
{
	smazPodstrom(koren);
}

void BinarniStrom::smazPodstrom(Uzel* podstrom)
{
	if (podstrom->levy != nullptr)
		smazPodstrom(podstrom->levy);
	if (podstrom->pravy != nullptr)
		smazPodstrom(podstrom->pravy);
	delete podstrom;
}

int main()
{
	BinarniStrom strom;

	strom.vloz(7);
	strom.vloz(4);
	strom.vloz(2);
	strom.vloz(6);
	strom.vloz(1);
	strom.vloz(5);
	strom.vloz(3);
	strom.vloz(10);
	strom.vloz(8);
	strom.vloz(12);
	strom.smaz(4);
	return 1;
}

