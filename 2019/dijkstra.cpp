// dijkstra.cpp.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <fstream>

struct Graf
{
	void nacti(std::fstream& f);

	int pocetVrcholu();

	int pocetSousedu(int vrchol);

	int soused(int vrchol, int indexSouseda, double* delkaHrany );

	//double delkaHrany(int i, int j);
};

enum Stav { znamy, neznamy, pokusny };


int Dijkstra(Graf* g, int start, int cil, int* cesta )
{
	int pocetVrcholu = g->pocetVrcholu();
	Stav* stavyVrcholu = new Stav[pocetVrcholu];
	double* delkyCest = new double[pocetVrcholu];
	for (int i = 0; i < pocetVrcholu; i++)
	{
		stavyVrcholu[i] = neznamy;
		delkyCest[i] = DBL_MAX; // "nekonecno"
	}
	delkyCest[start] = 0.0;
	stavyVrcholu[start] = znamy;
	int v = start;
	while (stavyVrcholu[cil] != znamy)
	{
		int pocetSouseduVrcholu = g->pocetSousedu(v);
		for (int i = 0; i < pocetSouseduVrcholu; i++)
		{
			double delka;
			int soused = g->soused(v, i, &delka);
			if (stavyVrcholu[soused] != znamy)
			{
				double tmp = delka + delkyCest[v];
				if (tmp < delkyCest[soused])
				{
					delkyCest[soused] = tmp;
					stavyVrcholu[soused] = pokusny;
				}
			}
		}
		int nejmensi = -1;
		for (int i = 0; i < pocetVrcholu; i++)
		{
			if (stavyVrcholu[i] == pokusny)
			{
				if (nejmensi == -1)
					nejmensi = i;
				else if (delkyCest[i] < delkyCest[nejmensi])
					nejmensi = i;
			}
		}
		v = nejmensi;
		stavyVrcholu[v] = znamy;
	}
	cesta[0] = cil;
	int pocet_vrcholu = 1;
	while (v != start)
	{	
		int pocet_sousedu = g->pocetSousedu(v);
		for (int i = 0; i < pocet_sousedu; i++)
		{
			double delka;
			int soused = g->soused(v, i, &delka);

			if (delkyCest[v] - delkyCest[soused] == delka)
			{
				cesta[pocet_vrcholu] = soused;
				v = soused;
				pocet_vrcholu++;
				break;
			}

				
		}
	}
	int l = 0;
	int p = pocet_vrcholu - 1;
	while (l < p)
	{
		int tmp = cesta[l];
		cesta[l] = cesta[p];
		cesta[p] = tmp;
		l++;
		p--;
	}
	return pocet_vrcholu;
}



int main()
{
 
}

