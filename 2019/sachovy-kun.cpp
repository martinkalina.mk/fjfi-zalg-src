#include "pch.h"
#include <iostream>

const int VelikostSachovnice;
bool sachovnice[VelikostSachovnice][VelikostSachovnice];
int cesta[VelikostSachovnice*VelikostSachovnice][2];
const int startRadek = 3;
const int startSloupec = 3;
int tahy[8][2] = {
	{-2,1},
	{-1,2},
	{}
	....
}

bool testSkoku(int radek, int sloupec)
{
	if (radek < VelikostSachovnice && radek >= 0 &&
		sloupec >= 0 && sloupec < VelikostSachovnice && 
		sachovnice[radek][sloupec] != true)
		return true;
	return false;
}

bool tahKonem(int radek, int sloupec, int delka_cesty)
{
	if (testSkoku(radek, sloupec) != true)
		return false;
	cesta[delka_cesty][0] = radek;
	cesta[delka_cesty][1] = sloupec;
	sachovnice[radek][sloupec] = true;
	if (delka_cesty == VelikostSachovnice * VelikostSachovnice - 1)
		return true;

	for (int i = 0; i < 8; i++)
		if (tahKonem(radek + tahy[i][0], sloupec + tahy[i][1], delka_cesty + 1))
			return true;
	sachovnice[radek][sloupec] = false;
}

int main()
{
	for (int i = 0; i < VelikostSachovnice; i++)
		for (int j = 0; j < VelikostSachovnice; j++)
			sachovnice[i][j] = false;

	tahKonem(startRadek, startSloupec, 0);
}

