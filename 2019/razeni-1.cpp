// razeni.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

const N = 100;

void primeVkladani(double a[N] )
{
	for (int j = 0; j < N-1; j++)
	{
		int index = j;
		for (int i = j+1; i < N; i++)
		{
			if (a[index] > a[i])
				index = i;
		}
		double pom;
		pom = a[j];
		a[j] = a[index];
		a[index] = pom;
	}
}

void primyVyber(double a[N])
{
	for (int i = 1; i < N; i++)
	{
		double pom = a[i];
		int j = i - 1;
		while (pom < a[j] && j>0 )
		{
			a[j+1] = a[j];
			j--;
		}
		a[j] = pom;
	}
}

void pretrasani(double a[N])
{
	int levy = 0, pravy = N, posledni;
	for (int i = levy; i < pravy; i++)
	{
		double tmp;
		if (a[i] > a[i + 1])
		{
			tmp = a[i];
			a[i] = a[i + 1];
			a[i + 1] = tmp;
			posledni = i + 1;
		}
	}
	pravy = posledni;
	for (int i = pravy-1; i > levy; i--)
	{
		double tmp;
		if (a[i] < a[i - 1])
		{
			tmp = a[i];
			a[i] = a[i - 1];
			a[i - 1] = tmp;
			posledni = i - 1;
		}
	}
	levy = posledni;
}

void quicksort(double a[N], int zacatek = 0, int konec = N)
{
	double pivot = a[konec-1];
	int leva = zacatek;
	int prava = konec - 2;
	while (leva < prava)
	{
		while (a[leva] < pivot && leva<prava)
			leva++;
		while (a[prava] > pivot && leva < prava)
			prava--;
		if (leva != prava)
		{
			double tmp = a[leva];
			a[leva] = a[prava];
			a[prava] = tmp;
		}
	}
	if (a[leva] < pivot)
	{
		a[konec - 1] = a[leva+1];
		a[leva+1] = pivot;
	}
	else
	{
		a[konec - 1] = a[leva];
		a[leva] = pivot;
	}
	if( zacatek < leva - 1 )
		quicksort(a, zacatek, leva);
	if( leva + 2 < konec )
		quicksort(a, leva + 1, konec);
}

int main()
{
	double a[N];
	quicksort(a);

}












































