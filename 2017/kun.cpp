const int velikost_sachovnice = 8;

const int skoky[8][2] =
{
   {-2,1},
   {-1,2},
   {1,2},
   {2,1},
   {2,-1},
   {1,-2},
   {-1,-2},
   {-2,-1}
};

struct poleSachovnice
{
   int radek, sloupec;
};

bool zkus_skok( int cislo_skoku, 
                poleSachovnice pozice, 
                bool sachovnice [velikost_sachovnice][velikost_sachovnice] )
{
   
}

bool skok_konem( poleSachovnice pozice, 
                 bool sachovnice [velikost_sachovnice][velikost_sachovnice],
                 int cislo_kroku, 
                 poleSachovnice* cesta )
{
   if( cislo_kroku == 63 )
      return true;
   for( int cislo_skoku = 0; cislo_skoku < 8; cislo_skoku++ )
      if(zkus_skok( cislo_skoku, pozice, sachovnice)==true)
      {
         poleSachovnice nova_pozice;
         nova_pozice.radek = pozice.radek+skoky[cislo_skoku][0];
         nova_pozice.sloupec = pozice.sloupec+skoky[cislo_skoku][1];
         sachovnice[nova_pozice.radek][nova_pozice.sloupec] = true;
         cesta[cislo_kroku] = nova_pozice;
         if(skok_konem(nova_pozice, sachovnice, cislo_kroku+1, cesta) == true)
            return true;
         sachovnice[nova_pozice.radek][nova_pozice.sloupec] = false;
      }
   return false;
}

void projdi_sachovnici( const poleSachovnice start,
                        poleSachovnice* cesta )
{
   assert( start.radek >= 0 );
   ...
   
   bool sachovnice [velikost_sachovnice][velikost_sachovnice];
   for (int i = 0; i < velikost_sachovnice; i++)
   {
      for (int j = 0; j < velikost_sachovnice; j++)
         sachovnice[i][j] = false;
   }
   sachovnice[start.radek][start.sloupec] = true;
   
   cesta [0] = start;
   
   skok_konem(start, sachovnice, 1, cesta);
}
