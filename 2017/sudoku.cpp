bool Zkus_cislo( const int pole[ 9 ][ 9 ], 
                 const int radek,
                 const int sloupec,
                 const int cislo )
{
   for (int i = 0; i <= 8; i++) {
      if(pole[i][sloupec] == cislo || pole[radek][i] == cislo)
         return false;
   }
   
   int radek_ctverce = (radek/3)*3;
   int sloupec_ctverce = (sloupec/3)*3;
   
   for (int i = radek_ctverce; i < radek_ctverce+3; i++){
      for (int j = sloupec_ctverce; j < sloupec_ctverce+3; j++){
         if(pole[i][j] == cislo)
            return false;
      }
   }
   return true;
}

bool resSudoku( const bool fixovana[ 9 ][ 9 ],
                int pole[ 9 ][ 9 ],
                int sloupec,
                int radek )
{
   if( sloupec == 9 ) 
   {
      if( radek == 8 )
         return true;
      sloupec = 0;
      radek=radek+1;
   }
   if( fixovana[radek][sloupec] == true )
      if( resSudoku(fixovana, pole, sloupec + 1, radek) )
         return true;
   else
   {
      for (int i=1;i<=9;i++)
         if( Zkus_cislo(pole,radek,sloupec,i) == true )
         {
            pole[radek][sloupec] = i;            
            if( resSudoku(fixovana, pole, sloupec + 1, radek ) == true )
               return true;
         }
      pole[radek][sloupec] = 0;
   }
   return false;
}

int main( int argc, char* argv[] )
{
   int pole[9][9];
   
   /*****
    * Policka oznacena jako 'true' jsou zadana, tj. zafixovana
    */
   bool fixovana[9][9];
   
   /****
    * Zadani sudoku
    */
   
   // ....
   
   resSudoku( fixovana, pole, 0, 0 );
}
