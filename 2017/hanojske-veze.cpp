#include <assert.h>

const int VyskaJehly = 100;

struct Jehla
{
   // Konstruktor nastavi pocet disku na 0 => jehla je prazdna.
   Jehla()
   {
      pocetDisku = 0;
   }

   void polozDisk( int rozmerDisku )
   {
      assert( pocetDisku >= 0 && pocetDisku < VyskaJehly - 1 );
      assert( pocetDisku == 0 || disky[ pocetDisku - 1 ] > rozmerDisku );
      
      disky[ pocetDisku ] = rozmerDisku;
      pocetDisku++;
   }
   
   int seberDisk()
   {
      assert( pocetDisku > 0 && pocetDisku < VyskaJehly );
      
      int disk = disky[ pocetDisku - 1 ];
      pocetDisku--;
      return disk;
   }
   
   void vypisJehlu()
   {
     // ...
   }
   
   
   int disky[ VyskaJehly ];
   
   int pocetDisku;
};

void PrenesDisky( Jehla* zdroj, Jehla* cil, Jehla* odkladaci, const int pocet )
{
   if( pocet == 1 )
   {
      cil->polozDisk( zdroj->seberDisk() );
      // vypis jehly
   }
   else
   {
      PrenesDisky( zdroj, odkladaci, cil, pocet - 1 );
      cil->polozDisk( zdroj->seberDisk() );
      // vypis jehly
      PrenesDisky( odkladaci, cil, zdroj, pocet - 1 );
   }
}

int main( int argc, char* argv[] )
{
   Jehla jehla[ 3 ];
   
   for( int i = 0; i < 15; i++ )
      jehla[ 0 ].polozDisk( 15 - i );
   
   PrenesDisky( &jehla[ 0 ], &jehla[ 1 ], &jehla[ 2 ], 15 );
}

