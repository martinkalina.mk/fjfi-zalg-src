#pragma once
  
struct Uzel
{
   Uzel();

   Uzel( int nova_hodnota );

   int hodnota;

   Uzel *levy, *pravy;
};

/**
 * \brief Struktura binarniho stromu.
 * 
 * TODO:
 * 1. Opravte funkci 'najdiVPodstromu' podle nize uvedeneho popisu.
 * 2. Implementujte funkce volane z funkce 'smaz'.
 * 3. Implementujte destructor, tj. smazani celeho stromu z pameti.
 */
struct BinarniStrom
{
   BinarniStrom();

   /**
    * @brief Funkce pro vlozeni prvku.
    *
    * @param hodnota je vladany prvek.
    */
   void vloz( int hodnota );

   bool najdi( int hodnota );

   bool smaz( int hodnota );

   bool ulozDoSouboru( char* nazevSoubor );

   ~BinarniStrom();

   void vlozDoPodstromu( Uzel* podstrom, int hodnota );

   /**
    * TODO: Implementujte tuto funkci tak, aby v zadanem podstromu 'podstrom'
    * hledala uzel s hodnodotou 'hodnota'. Pokud takovu uzel najde, vrati nam ukazatel na nej
    * pomoci ukazatele na ukazatel 'hledany' a stejnym zpusobem preda i jeho predchudce
    * 'predchudce'.Funkce vraci 'true', pokud byl uzel s danou hodnotu nalezen, jinak vraci 'false'.
    * 
    * \param podstrom je podstrom, ve kterem se hleda zadana 'hodnota'
    * \param hodnota je hledana 'hodnota'
    * \param hledany je ukazatel na uzel, kde je hledana hodnota, pokud byla nalezena 
    * \param predchudce je ukazatel na predchudce uzlu s hledanou hodnotou
    * 
    * \return vraci 'true' pokud byla hodnota nalezena, jinak vraci 'false'
    */
   bool najdiVPodstromu( Uzel* podstrom, int hodnota, Uzel** hledany, Uzel** predchudce );

   void smazList( Uzel* mazany, Uzel* predchudce );

   void smazVeVetvi( Uzel* mazany, Uzel* predchudce );

   void smazVeStromu( Uzel* mazany, Uzel* predchudce );
   
   Uzel* koren;
};



















