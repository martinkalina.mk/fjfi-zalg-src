/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



void serad( int a, int b, int* mensi, int* vetsi )
{
   if( a < b ) 
   {
      *mensi = a;
      *vetsi = b;
   }
   else
   {
      *mensi = b;
      *vetsi = a;
   }
}

void prohod( int* a, int* b )
{
   int c = *a;
   *a = *b;
   *b = c;
}

void prohodUkazatele( int** a, int** b )
{
   int* c = *a;
   *a = *b;
   *b = c;
}



int main( int argc, char* argv[] )
{
   int a( 3 ), b( 5 ), vetsi, mensi;
   serad( a, b, &mensi, &vetsi );
   
   int *mensi2, *vetsi2;
   serad( a, b, mensi2, vetsi2 );
   
   prohod( &a, &b );
   
   prohodUkazatele( &mensi2, &vetsi2 );
}