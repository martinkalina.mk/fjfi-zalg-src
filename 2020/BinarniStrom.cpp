#include <assert.h>
#include "BinarniStrom.h"

Uzel::Uzel()
{
   pravy = nullptr;
   levy = nullptr;
}

Uzel::Uzel( int nova_hodnota )
{
   pravy = nullptr;
   levy = nullptr;
   hodnota = nova_hodnota;

}

BinarniStrom::BinarniStrom()
{
   koren = nullptr;
}

void BinarniStrom::vloz( int hodnota )
{
   if(koren == nullptr)
      koren = new Uzel( hodnota );
   else
      vlozDoPodstromu( koren, hodnota );
}

void BinarniStrom::vlozDoPodstromu( Uzel* podstrom, int hodnota )
{
   assert( podstrom != nullptr );
   if (hodnota > podstrom->hodnota)
   {
      if(podstrom->pravy == nullptr)
         podstrom->pravy = new Uzel (hodnota);
      else vlozDoPodstromu (podstrom->pravy, hodnota);
   }
   else
   {
      if(podstrom->levy == nullptr)
         podstrom->levy = new Uzel (hodnota);
      else vlozDoPodstromu (podstrom->levy, hodnota);
   }
}

bool BinarniStrom::najdi(int hodnota)
{
   ////
   // Tyto promenne tu nepotrenujem. Funcke najdiVPodStromu ale funguje
   // i pro obecnejsi ucely, pro ktere jsou tyto ukazatele potreba.
   Uzel *hledany, *predchudce;
   return najdiVPodstromu( koren, hodnota, &hledany, &predchudce );
}

bool BinarniStrom::najdiVPodstromu( Uzel* podstrom, int hodnota, Uzel** hledany, Uzel** predchudce )
{
   if( podstrom == nullptr )
      return false;
   if( hodnota == podstrom->hodnota )
      return true;
   if( hodnota > podstrom->hodnota )
      return najdiVPodstromu( podstrom->pravy, hodnota );
   else
      return najdiVPodstromu( podstrom->levy, hodnota );
}

bool BinarniStrom::smaz( int hodnota )
{
   Uzel *hledany, *predchudce;
   if( !najdiVPodstromu( koren, hodnota, &hledany, &predchudce ) )
      return false;

   ////
   // V ukazateli 'hledany' je ukazatel na uzel, kde je nalzena hodnota,
   // v ukazateli 'predchudce' je jeho predchudce.
   if( hledany->levy == nullptr && hledany->pravy == nullptr )
   {
      smazList( hledany, predchudce );
   }
   else if( hledany->levy == nullptr || hledany->pravy == nullptr )
   {
      smazVeVetvi( hledany, predchudce );
   }
   else
   {
      smazVeStromu( hledany, predchudce );
   }
   return true;
}

void BinarniStrom::smazList( Uzel* mazany, Uzel* predchudce )
{
   
}

void BinarniStrom::smazVeVetvi( Uzel* mazany, Uzel* predchudce )
{
   
}

void BinarniStrom::smazVeStromu( Uzel* mazany, Uzel* predchudce )
{
   
}

BinarniStrom::~BinarniStrom()
{
   // TODO: Implementujte smazani celeho stromu z pameti
}