const int N = 8;
const int startovniRadek = 3;
const int startovniSloupec = 2;

struct Pozice
{
    int radek, sloupec;
};

int skoky[ 8 ][ 2 ] = { { 1,  2},
                        { 2,  1},
                        { 2, -1},
                        { 1, -2},
                        {-1, -2},
                        {-2, -1},
                        {-2,  1},
                        {-1,  2}
};

void jizdaKonem( Pozice aktualniPozice,
                 bool sachovnice[ N ][ N ],
                 Pozice cesta[ N * N ],
                 int cisloSkoku )
{
    sachovnice[aktualniPozice.radek][aktualniPozice.sloupec] = true;
    cesta[cisloSkoku] = aktualniPozice;
    
    if (cisloSkoku == N*N-1) {
        vypisCestu(cesta);
        return;
    }
    
    for (int i=0; i< 8; i++) {
        Pozice novaPozice;
        novaPozice.radek = aktualniPozice.radek + skoky[i][0];
        novaPozice.sloupec = aktualniPozice.sloupec + skoky[i][1];
        
        if (novaPozice.radek < 0 || novaPozice.radek > 7 || novaPozice.sloupec < 0 || novaPozice.sloupec > 7 )
            continue;
        
        if (sachovnice[novaPozice.radek][novaPozice.sloupec] == true)
            continue;
        
        jizdaKonem( novaPozice, sachovnice, cesta, cisloSkoku+1);
    }
    sachovnice[aktualniPozice.radek][aktualniPozice.sloupec] = false;
}

int main( int argc, char* argv[] )
{
    bool sachovnice[ N ][ N ];
    Pozice cesta[ N * N ];
    Pozice start;
    start.radek = startovniRadek;
    start.sloupec = startovniSloupec;
    cesta[ 0 ] = start;
    
    for( int i = 0; i < N; i++ )
        for( int j = 0; j < N; j++ )
            sachovnice[ i ][ j ] = false;
    
    jizdaKonem( start, sachovnice, cesta, 0 );
}
