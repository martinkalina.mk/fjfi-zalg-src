/* 
 * File:   graf.h
 * Author: Tomas Oberhuber
 *
 * Created on 12. dubna 2018, 10:44
 */

#ifndef GRAF_H
#define	GRAF_H

struct Graf
{
    
    bool nacti( const char* soubor );
    
    int pocetVrcholu();
    
    int pocetSousedu( int vrchol );
    
    int soused( int vrchol, int hrana );
    
    double delkaHrany( int vrchol, int hrana );
}

#endif	/* GRAF_H */

