void primyVyber( double* a, int N )
{
   for (int i=0; i<N-1; i++) {
      int index = i;
      for (int j=i+1; j<N; j++) {
         if (a[j] < a[index]) {
            index = j;
         }
      }
      prohod(a[i], a[index]);
   }
}

void primeVkladani( double* a, int N )
{
   for(int k = 1;k<N;k++)
   {
      double cislo = a[k];
      for(int i = k; i >= 0; i--)
      {
         if(cislo < a[i])
         {
            a[i+1]=a[i];
         }
         else
         {
            a[i+1]=cislo;
            break;
         }
      }
   }
}

void primeVkladani2( double* a, int N )
{
   for(int k = 1;k<N;k++)
   {
      double cislo = a[k];
      int i = k;
      while( i>=0 && cislo < a[i] )
      {
         a[i]=a[i-1];
         i--;
      }
      a[i+1]=cislo;
   }
}

void primeVkladani3( double* a, int N )
{
   for(int k = 1;k<N;k++)
   {
      double cislo = a[k];
      
      int leva = 0;
      int prava = k-1;
      while( leva < prava-1 )
      {
         int pulka = ( leva + prava ) / 2;
         if( a[ pulka ] > cislo )
            prava = pulka;
         else
            leva = pulka;
      }
      for( int i = k; i > prava; i--)
         a[i]=a[i-1];
      a[prava] = cislo;
   }
}




