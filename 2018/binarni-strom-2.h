#ifndef BINARNI_STROM_H
#define	BINARNI_STROM_H

struct Uzel
{   
    double data;
    
    Uzel *levy, *pravy;
};

struct Strom
{
    Strom();
    
    bool vloz( double data );
    
    bool najdi( double data );
    
    bool smaz( double data );
    
    bool zapis( const char* soubor );
    
    bool nacti( const char* soubor );
    
    ~Strom();
    
    // Pomocne metody
    
    vlozDoPodstromu( Uzel* podstrom, double data );
    
    Uzel* koren;
};


#endif	/* BINARNI_STROM_H */

