## Základní nastavení

### Instalace Linuxu

Zporovozněte si na svém počítačí Linux. Pokud nemáte, nejjednodušší možností je virtualizace. Doporučuji využít [VirtualBox](https://www.virtualbox.org/). Ten vám umožní spustit si ve vašem operačním systému jiný opoerační systém. Do VirtualBoxu si tedy nainstalujte některou distribuci Linuxu, doporučuji [Ubuntu](https://ubuntu.com/download/desktop)
 
### Potřebné programy

Budeme potřebovat zejména překladač jazyka C++. Nejčastěji používaný je program `g++`. V Ubuntu ho nainstalujete příkazem:

```
sudo apt install g++-8-multilib
```

Dále budete potřebovat program `git` pomocí něhož si budete moci stahovat aktuální zdrojové kódy z tohoto repozitáře, ale také do něj vkládat Vaše vlastní kódy. Tento program nainstalujete poříkazem

```
sudo apt install git
```

Dále ještě budete potřebovat nějaký textový editor nebo vývojové prostředí (IDE) pro psaní kódu. Možnosti jsou:

1. Kate (editor): `sudo apt install kate`
2. QtCreator (IDE): `sudo apt install qtcreator`
3. Codeblocks (IDE): `sudo apt install codeblocks`
4. Visual Studio Code (IDE): [https://visualstudio.microsoft.com/cs/](https://visualstudio.microsoft.com/cs/)

### Nastavení Gitlabu

Pomocí [tohoto odkazu](https://gitlab.com/profile/keys) si přidejte svůj SSH klíč podle návodu.

### Stažení zdrojových kódů

Zdrojové kódy si stáhnětě pomocí příkazu:

```
git clone git@gitlab.com:oberhuber.tomas/fjfi-zalg-src.git
```

Ve vzniklem adresari jsou zdrojové kódy z předchozích let. Můžete je využít jako nápovědu, při řešení úkolů. V adresáři `2020` jsou naše současné programy. Pokud zadáte příkaz

```
make
```

měl by vám vzniknout mimo jiné program `binarni-strom-test`, který můžete spustit.

### Nastavení vlastní větve

Repozitář umožňuje více lidem pracovat na jednom projektu. Za tím účelem si každý vývojář vytvoří jednu nebo více větví. Pro každého z vás už je jedna větev vytvořena. Její název má tvar `2020-JmenoPrijmeni`. Seznam všech větví je [zde](https://gitlab.com/oberhuber.tomas/fjfi-zalg-src/-/branches/all). Do vaší větve se přepnete takto (musíte být v adresáři, který vznikl po příkazu `git clone...`):

```
git checkout -b 2020-JmenoPrijmeni
```

```
git branch --set-upstream-to=origin/2020-JmenoPrijmeni 2020-JmenoPrijmeni
```

Následně můžete začít pracovat na zadaných úkolech. Jednotlivé změny vkládáte do gitu pomocí příkazu:
```
git commit -a -m 'Popisek provedenych zmen.'
```

Následně je pak potřeba je odeslat do repozitáře na Gitlabu pomocí přííkazu:

```
git push
```

Pokud vám dám vědět, že jsem provedl nějaké změny, tj. vyvtořil další zadání, stáhnete si je pomocí příkazu:

```
git pull
```

